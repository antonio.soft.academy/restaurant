import com.restaurant.customer.Customer;
import com.restaurant.table.Table;
import com.restaurant.waiter.Waiter;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Customer restaurant = new Customer("212310012312", "Test Restaurant EOOD",
                "Sofia, Bulgaria", "Soft Academy", "+359-99-99-99-99");

        Scanner sc = new Scanner(System.in);

        System.out.println("Your ID number");
        String idNumber = sc.nextLine();

        Waiter.CheckWaiter(idNumber);

        Waiter assignedWaiterName = Waiter.Names(idNumber);

        System.out.println("Enter table number: ");
        int tableNumber = sc.nextInt();

        Table.CheckTableExistance(tableNumber);




    }
}