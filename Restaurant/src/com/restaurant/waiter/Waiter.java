package com.restaurant.waiter;

import java.io.*;

public class Waiter {
    static final String WAITERS_TABLE_PATH = "C:\\Users\\Antonio\\Desktop\\Restaurant\\restaurant\\Restaurant\\DataBase\\Waiters.txt";
    private String Name;

    public Waiter(String name){
        this.Name = name;
    }

    public String getName(){
        return this.Name;
    }

    public static void CheckWaiter(String inputWaiterID) throws IOException {
        String line;
        boolean isFound = false;

        File waiters = new File(WAITERS_TABLE_PATH);
        BufferedReader readWaiter = new BufferedReader(new FileReader(waiters));

        while ((line = readWaiter.readLine() ) != null){
            String[] waiterRow = line.split(",");

            if (waiterRow[0].equals(inputWaiterID)){
                isFound = true;
                break;
            }
        }

        readWaiter.close();

        if(isFound == true){
            System.out.println("Welcome to our Restaurant sytem");
        }
        else {
            System.out.println("Incorrect ID try again!!!");
            System.exit(0);
        }

    }

    public static Waiter Names(String waiterID) throws IOException {
        String line;
        String waiterName = null;

        File waiters = new File(WAITERS_TABLE_PATH);
        BufferedReader readWaiter = new BufferedReader(new FileReader(waiters));

        while ((line = readWaiter.readLine() ) != null){
            String[] waiterRow = line.split(",");

            if (waiterRow[0].equals(waiterID)){
                waiterName = waiterRow[1];
                break;
            }
        }

        readWaiter.close();

        return new Waiter(waiterName);

    }




}

