package com.restaurant.customer;

public class Customer {
    private final String EIK;
    private final String Name;
    private final String Address;
    private final String ManagerName;
    private final String ManagerPhone;



    public Customer(String eik, String name, String address,
                    String managerName, String managerPhone) {

        this.EIK = eik;
        this.Name = name;
        this.Address = address;
        this.ManagerName = managerName;
        this.ManagerPhone = managerPhone;
    }
    public String getEIK(){
        return this.EIK;
    }

    public String getName(){
        return this.Name;
    }

    public String getAddress(){
        return this.Address;
    }

    public String getManagerName(){
        return this.ManagerName;
    }

    public String getManagerPhone(){
        return this.ManagerPhone;
    }

}
