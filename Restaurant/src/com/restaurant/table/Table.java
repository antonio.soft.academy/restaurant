package com.restaurant.table;

import java.io.File;

public class Table {

    static final String TABLES_PATH = "C:\\Users\\Antonio\\Desktop\\Restaurant\\restaurant\\Restaurant\\DataBase\\Tables\\";

    public  static  void CheckTableExistance(int tableNumber){
        File tableFile = new File(TABLES_PATH.concat(tableNumber + ".txt"));
        boolean isExist = tableFile.exists();

        if(isExist == true){
            System.out.println("Table has clients on it");
        }
        else{
            System.out.println("There is no this kind of table");
        }
    }
}
